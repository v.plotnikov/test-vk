package com.vk.internship

import android.content.Context
import kotlin.math.ceil

object AndroidUtilities {
    const val applicationTag = "VkInternship"

    const val databaseName = "groups.db"

    var statusBarHeight = 0
    var navigationBarHeight = 0
    private var density = 0f

    fun fillStatusBarHeight(context: Context) {
        if (statusBarHeight > 0) {
            return
        }
        val resourceId: Int =
            context.resources.getIdentifier("status_bar_height", "dimen", "android")
        if (resourceId > 0) {
            statusBarHeight = context.resources.getDimensionPixelSize(resourceId)
        }
    }

    fun fillNavigationBarHeight(context: Context) {
        if (navigationBarHeight > 0) {
            return
        }
        val resourceId: Int =
            context.resources.getIdentifier("navigation_bar_height", "dimen", "android")
        if (resourceId > 0) {
            navigationBarHeight = context.resources.getDimensionPixelSize(resourceId)
        }
    }

    fun checkDisplaySize(context: Context) {
        density = context.resources.displayMetrics.density
    }

    fun dp(value: Float): Int {
        if (value == 0f) {
            return 0
        }
        return ceil(density * value).toInt()
    }
}