package com.vk.internship.ui

import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.transition.AutoTransition
import android.transition.TransitionManager
import android.view.View
import android.view.animation.AccelerateInterpolator
import android.view.animation.AlphaAnimation
import android.view.animation.DecelerateInterpolator
import android.widget.FrameLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SwitchCompat
import androidx.constraintlayout.motion.widget.MotionLayout
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import androidx.core.view.WindowCompat
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.room.Room
import com.vk.api.sdk.VK
import com.vk.api.sdk.VKApiCallback
import com.vk.dto.common.id.UserId
import com.vk.internship.AndroidUtilities
import com.vk.internship.R
import com.vk.internship.adapters.GroupsAdapter
import com.vk.internship.databases.GroupModelsDatabase
import com.vk.internship.models.GroupModel
import com.vk.internship.ui.components.ButtonWithCounter
import com.vk.internship.viewmodels.GroupsViewModel
import com.vk.sdk.api.base.dto.BaseOkResponse
import com.vk.sdk.api.groups.GroupsService
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class GroupsActivity : AppCompatActivity() {
    private lateinit var database: GroupModelsDatabase

    private lateinit var root: ConstraintLayout
    private lateinit var motionChild: MotionLayout
    private val motionChildTag = "motionChild"

    private lateinit var modeSwitch: SwitchCompat

    private lateinit var groupsViewModel: GroupsViewModel
    private lateinit var groupsView: RecyclerView
    private lateinit var decoration: GroupsListDecoration

    private lateinit var subscribedAdapter: GroupsAdapter
    private lateinit var unsubscribedAdapter: GroupsAdapter

    private var spanCount: Int = 0

    private lateinit var actionLayout: FrameLayout
    private lateinit var actionButton: ButtonWithCounter

    private lateinit var dimLayout: FrameLayout
    private lateinit var selectionMenu: ConstraintLayout

    private var isSelectionMenuOpen = false
    private var isSelectionMenuOpenTag = "isSelectionMenuOpen"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_groups)

        WindowCompat.setDecorFitsSystemWindows(window, false)
        window.statusBarColor = Color.TRANSPARENT
        window.navigationBarColor = Color.TRANSPARENT

        database = Room.databaseBuilder(
            this,
            GroupModelsDatabase::class.java,
            AndroidUtilities.databaseName
        ).allowMainThreadQueries().build()

        root = findViewById(R.id.activity_groups)
        motionChild = findViewById(R.id.activity_groups_motion)
        motionChild.setPadding(
            motionChild.paddingStart,
            AndroidUtilities.statusBarHeight,
            motionChild.paddingEnd,
            motionChild.paddingBottom
        )

        groupsViewModel = ViewModelProvider(this)[GroupsViewModel::class.java]

        groupsViewModel.setUnsubscribedGroups(database.GroupModelsDao().getAll())

        modeSwitch = findViewById(R.id.activity_groups_mode)
        modeSwitch.setOnCheckedChangeListener { _, b ->
            val title = findViewById<TextView>(R.id.activity_groups_title)
            groupsViewModel.setActivityMode(
                if (b) {
                    title.text = resources.getString(R.string.subscribe_title)
                    actionButton.text = resources.getString(R.string.subscribe)

                    GroupsViewModel.Companion.ActivityMode.SUBSCRIBE
                } else {
                    title.text = resources.getString(R.string.unsubscribe_title)
                    actionButton.text = resources.getString(R.string.unsubscribe)

                    GroupsViewModel.Companion.ActivityMode.UNSUBSCRIBE
                }
            )
        }

        spanCount =
            if (resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT) 3 else 6

        groupsView = findViewById(R.id.activity_groups_list)
        groupsView.layoutManager = GridLayoutManager(this, spanCount)

        decoration = GroupsListDecoration(spanCount, AndroidUtilities.dp(8f))
        groupsView.addItemDecoration(decoration)

        dimLayout = findViewById(R.id.activity_groups_dim)
        selectionMenu = findViewById(R.id.group_selection_menu)
        selectionMenu.setPadding(
            selectionMenu.paddingStart,
            selectionMenu.paddingTop,
            selectionMenu.paddingEnd,
            selectionMenu.paddingBottom + AndroidUtilities.navigationBarHeight
        )

        subscribedAdapter = GroupsAdapter(this)
        subscribedAdapter.onClickListener = {
            val count = groupsViewModel.getSelectedGroupsCount().value!!
            if (it.isSelected) {
                groupsViewModel.setSelectedGroupsCount(count + 1)
            } else {
                groupsViewModel.setSelectedGroupsCount(count - 1)
            }
        }
        subscribedAdapter.onLongClickListener = {
            openSelectionMenu(it)
        }

        groupsView.adapter = subscribedAdapter

        unsubscribedAdapter = GroupsAdapter(this)
        unsubscribedAdapter.onClickListener = {
            val count = groupsViewModel.getSelectedGroupsCount().value!!
            if (it.isSelected) {
                groupsViewModel.setSelectedGroupsCount(count + 1)
            } else {
                groupsViewModel.setSelectedGroupsCount(count - 1)
            }
        }
        unsubscribedAdapter.onLongClickListener = {
            openSelectionMenu(it)
        }

        groupsViewModel.getActivityMode().observe(this) { activityMode ->
            when (activityMode) {
                GroupsViewModel.Companion.ActivityMode.UNSUBSCRIBE -> {
                    groupsViewModel.getSubscribedGroups().observe(this) {
                        groupsView.adapter = subscribedAdapter
                        subscribedAdapter.setGroups(it)
                    }
                }
                else -> {
                    groupsViewModel.getUnsubscribedGroups().observe(this) {
                        groupsView.adapter = unsubscribedAdapter
                        unsubscribedAdapter.setGroups(it)
                    }
                }
            }
        }

        actionLayout = findViewById(R.id.activity_groups_action_layout)
        actionLayout.setPadding(0, 0, 0, AndroidUtilities.navigationBarHeight)

        actionButton = findViewById(R.id.activity_groups_action)
        actionButton.setOnClickListener {
            var adapter = subscribedAdapter
            val moveToContainer =
                if (groupsViewModel.getActivityMode().value == GroupsViewModel.Companion.ActivityMode.UNSUBSCRIBE) {
                    groupsViewModel.getUnsubscribedGroups().value
                } else {
                    adapter = unsubscribedAdapter
                    groupsViewModel.getSubscribedGroups().value
                }
            val groups = adapter.getGroups()

            for (i in groups.size - 1 downTo 0) {
                val group = groups[i]
                if (group.isSelected) {
                    group.isSelected = false

                    adapter.removeGroup(i)

                    when (groupsViewModel.getActivityMode().value) {
                        GroupsViewModel.Companion.ActivityMode.UNSUBSCRIBE -> {
                            leaveGroup(group, moveToContainer, adapter)
                        }
                        else -> {
                            joinGroup(group, moveToContainer, adapter)
                        }
                    }
                }
            }

            groupsViewModel.setSelectedGroupsCount(0)
        }

        dimLayout.setOnClickListener {
            closeSelectionMenu()
        }

        if (savedInstanceState != null) {
            if (savedInstanceState.getBoolean(isSelectionMenuOpenTag)) {
                openSelectionMenu(groupsViewModel.getSelectedGroup())
            }
        }

        groupsViewModel.getSelectedGroupsCount().observe(this) {
            val handler = Handler(Looper.getMainLooper())
            handler.post {
                if (it > 0) {
                    actionButton.counter = it

                    decoration.setLastItemsMargin(actionLayout.height)
                    groupsView.invalidateItemDecorations()
                    actionLayout.visibility = View.VISIBLE
                } else {
                    decoration.setLastItemsMargin(AndroidUtilities.navigationBarHeight)
                    actionLayout.visibility = View.INVISIBLE
                }
            }
        }
    }

    private fun leaveGroup(
        group: GroupModel,
        container: ArrayList<GroupModel>?,
        adapter: GroupsAdapter
    ) {
        VK.execute(
            GroupsService().groupsLeave(UserId(group.id)),
            object : VKApiCallback<BaseOkResponse> {
                override fun success(result: BaseOkResponse) {
                    container?.add(group)
                    database.GroupModelsDao().insert(group)
                }

                override fun fail(error: Exception) {
                    adapter.insertGroup(group)

                    Toast.makeText(
                        this@GroupsActivity,
                        resources.getString(R.string.group_unsubscribe_action_error, group.name),
                        Toast.LENGTH_SHORT
                    ).show()
                }
            })
    }

    private fun joinGroup(
        group: GroupModel,
        container: ArrayList<GroupModel>?,
        adapter: GroupsAdapter
    ) {
        VK.execute(
            GroupsService().groupsJoin(UserId(group.id)),
            object : VKApiCallback<BaseOkResponse> {
                override fun success(result: BaseOkResponse) {
                    container?.add(group)
                    database.GroupModelsDao().delete(group)
                }

                override fun fail(error: Exception) {
                    adapter.insertGroup(group)

                    Toast.makeText(
                        this@GroupsActivity,
                        resources.getString(R.string.group_subscribe_action_error, group.name),
                        Toast.LENGTH_SHORT
                    ).show()
                }
            })
    }

    private enum class Case {
        NOMINATIVE_SINGULAR, NOMINATIVE_PLURAL, GENITIVE_PLURAL
    }

    private fun openSelectionMenu(group: GroupModel?) {
        if (group != null) {
            groupsViewModel.setSelectedGroup(group)

            if (!group.name.isNullOrEmpty()) {
                root.findViewById<TextView>(R.id.group_selection_menu_name).text = group.name
            }

            if (!group.friendsCount.hasObservers()) {
                group.friendsCount.observe(this) {
                    root.findViewById<TextView>(R.id.group_selection_menu_stats).text =
                        resources.getString(
                            R.string.group_stats,
                            if (group.membersCount >= 1000) "%.1fK".format(group.membersCount / 1000f) else group.membersCount.toString(),
                            when (getNumberCase(group.membersCount)) {
                                Case.NOMINATIVE_SINGULAR -> {
                                    resources.getString(R.string.group_subs_nominative_singular)
                                }
                                Case.NOMINATIVE_PLURAL -> {
                                    resources.getString(R.string.group_subs_nominative_plural)
                                }
                                Case.GENITIVE_PLURAL -> {
                                    resources.getString(R.string.group_subs_genitive_plural)
                                }
                            },
                            group.friendsCount.value!!,
                            when (getNumberCase(group.friendsCount.value!!)) {
                                Case.NOMINATIVE_SINGULAR -> {
                                    resources.getString(R.string.group_subs_friends_nominative_singular)
                                }
                                Case.NOMINATIVE_PLURAL -> {
                                    resources.getString(R.string.group_subs_friends_nominative_plural)
                                }
                                Case.GENITIVE_PLURAL -> {
                                    resources.getString(R.string.group_subs_friends_genitive_plural)
                                }
                            },
                        )
                }
            }

            val description = group.description?.substringBefore("\n\n")
            if (!description.isNullOrEmpty()) {
                root.findViewById<TextView>(R.id.group_selection_menu_desc).text = description
            }

            if (!group.lastPostDate.hasObservers()) {
                group.lastPostDate.observe(this) {
                    if (group.lastPostDate.value!! > 0) {
                        val simpleDate = SimpleDateFormat("d MMMM", Locale.getDefault())
                        root.findViewById<TextView>(R.id.group_selection_menu_post).text =
                            resources.getString(
                                R.string.group_post,
                                simpleDate.format(Date(group.lastPostDate.value!! * 1000L))
                            )
                    }
                }
            }

            groupsViewModel.getFriendsCountAndLastPostDate(group)

            root.findViewById<FrameLayout>(R.id.group_selection_menu_close).setOnClickListener {
                closeSelectionMenu()
            }

            isSelectionMenuOpen = true

            dimLayout.visibility = View.VISIBLE
            val alphaAnimation = AlphaAnimation(0f, 1f)
            alphaAnimation.duration = 200
            dimLayout.startAnimation(alphaAnimation)

            val constraintSet = ConstraintSet()
            constraintSet.clone(root)
            constraintSet.clear(R.id.group_selection_menu, ConstraintSet.TOP)
            constraintSet.connect(
                R.id.group_selection_menu,
                ConstraintSet.BOTTOM,
                R.id.activity_groups,
                ConstraintSet.BOTTOM
            )
            val autoTransition = AutoTransition()
            autoTransition.duration = 200
            autoTransition.interpolator = DecelerateInterpolator()
            TransitionManager.beginDelayedTransition(root, autoTransition)
            constraintSet.applyTo(root)
        }
    }

    private fun getNumberCase(number: Int): Case {
        if (number / 10 == 1 || number >= 1000) {
            return Case.GENITIVE_PLURAL
        }
        return when (number % 10) {
            1 -> {
                Case.NOMINATIVE_SINGULAR
            }
            2, 3, 4 -> {
                Case.NOMINATIVE_PLURAL
            }
            else -> {
                Case.GENITIVE_PLURAL
            }
        }
    }

    private fun closeSelectionMenu() {
        isSelectionMenuOpen = false

        val alphaAnimation = AlphaAnimation(1f, 0f)
        alphaAnimation.duration = 150
        dimLayout.startAnimation(alphaAnimation)
        dimLayout.visibility = View.INVISIBLE

        val constraintSet = ConstraintSet()
        constraintSet.clone(root)
        constraintSet.clear(R.id.group_selection_menu, ConstraintSet.BOTTOM)
        constraintSet.connect(
            R.id.group_selection_menu,
            ConstraintSet.TOP,
            R.id.activity_groups,
            ConstraintSet.BOTTOM
        )
        val autoTransition = AutoTransition()
        autoTransition.duration = 150
        autoTransition.interpolator = AccelerateInterpolator()
        TransitionManager.beginDelayedTransition(root, autoTransition)
        constraintSet.applyTo(root)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        outState.putBundle(motionChildTag, motionChild.transitionState)
        outState.putBoolean(isSelectionMenuOpenTag, isSelectionMenuOpen)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)

        motionChild.transitionState = savedInstanceState.getBundle(motionChildTag)
        isSelectionMenuOpen = savedInstanceState.getBoolean(isSelectionMenuOpenTag)
    }

    override fun onBackPressed() {
        if (isSelectionMenuOpen) {
            closeSelectionMenu()
        } else {
            super.onBackPressed()
        }
    }
}