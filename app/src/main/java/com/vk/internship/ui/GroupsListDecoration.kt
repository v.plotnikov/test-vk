package com.vk.internship.ui

import android.graphics.Rect
import android.util.Log
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.vk.internship.AndroidUtilities

class GroupsListDecoration(
    private val spanCount: Int,
    private val padding: Int,
    private var lastItemsMargin: Int = AndroidUtilities.navigationBarHeight
) :
    RecyclerView.ItemDecoration() {
    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: RecyclerView,
        state: RecyclerView.State
    ) {
        super.getItemOffsets(outRect, view, parent, state)

        val position = parent.getChildAdapterPosition(view)

        var paddingBottom = 0

        val adapter = parent.adapter
        if (adapter != null && adapter.itemCount > 0) {
            if (position.div(spanCount) == (adapter.itemCount - 1).div(spanCount)) {
                paddingBottom = lastItemsMargin
            }
        }

        when {
            position % spanCount == 0 -> {
                outRect.set(padding, 0, padding / 6, paddingBottom)
            }
            position % spanCount == (spanCount - 1) -> {
                outRect.set(padding / 6, 0, padding, paddingBottom)
            }
            position != RecyclerView.NO_POSITION -> {
                outRect.set(padding / 6, 0, padding / 6, paddingBottom)
            }
        }
    }

    fun setLastItemsMargin(margin: Int) {
        if (margin >= 0) {
            lastItemsMargin = margin
        }
    }
}