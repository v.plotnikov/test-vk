package com.vk.internship.ui

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.WindowCompat
import com.vk.api.sdk.VK
import com.vk.api.sdk.auth.VKAuthenticationResult
import com.vk.api.sdk.auth.VKScope
import com.vk.internship.AndroidUtilities
import com.vk.internship.R
import com.vk.internship.ui.components.ButtonWithCounter

class StartActivity : AppCompatActivity() {
    private lateinit var loginButton: ButtonWithCounter

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)

        if (VK.isLoggedIn()) {
            launchGroupsActivity()
        } else {
            val dbName = getDatabasePath(AndroidUtilities.databaseName)
            if (dbName.exists()) {
                dbName.delete()
            }

            setContentView(R.layout.activity_start)

            WindowCompat.setDecorFitsSystemWindows(window, false)
            window.statusBarColor = Color.TRANSPARENT
            window.navigationBarColor = Color.TRANSPARENT

            val root = findViewById<ConstraintLayout>(R.id.activity_start)
            root.setPadding(
                root.paddingStart,
                AndroidUtilities.statusBarHeight,
                root.paddingEnd,
                root.paddingTop
            )

            val authLauncher = VK.login(this) { result: VKAuthenticationResult ->
                when (result) {
                    is VKAuthenticationResult.Success -> {
                        launchGroupsActivity()
                    }
                    is VKAuthenticationResult.Failed -> {
                        Toast.makeText(this, R.string.login_error, Toast.LENGTH_SHORT).show()

                        loginButton.isClickable = true
                    }
                }
            }

            loginButton = findViewById(R.id.activity_start_login)
            loginButton.setOnClickListener {
                loginButton.isClickable = false

                authLauncher.launch(arrayListOf(VKScope.GROUPS))
            }
        }
    }

    private fun launchGroupsActivity() {
        startActivity(Intent(this, GroupsActivity::class.java))
        finish()
    }

    companion object {
        fun startFrom(context: Context) {
            val intent = Intent(context, StartActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP
            context.startActivity(intent)
        }
    }
}