package com.vk.internship.ui.components

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.vk.internship.R

class ButtonWithCounter(context: Context, attrs: AttributeSet) : LinearLayout(context, attrs),
    View.OnClickListener {
    private val view: View = inflate(context, R.layout.button_with_counter, this)

    private val root: LinearLayout = view.findViewById(R.id.button_with_counter)
    private val titleView: TextView = view.findViewById(R.id.button_with_counter_title)
    private val counterView: TextView = view.findViewById(R.id.button_with_counter_value)

    private var listener: OnClickListener? = null

    var text: CharSequence
        set(value) {
            titleView.text = value
        }
        get() {
            return titleView.text
        }

    var counter = 0
        set(value) {
            if (value > 0) {
                field = value

                counterView.text = value.toString()
                counterView.visibility = VISIBLE
            } else {
                field = 0
                counterView.visibility = GONE
            }
        }

    init {
        val typedArray = context.obtainStyledAttributes(attrs, R.styleable.ButtonWithCounter, 0, 0)
        titleView.text = typedArray.getString(R.styleable.ButtonWithCounter_title)
        typedArray.recycle()

        root.setOnClickListener(this)
    }

    override fun setOnClickListener(l: OnClickListener?) {
        listener = l
    }

    override fun onClick(view: View?) {
        listener?.onClick(view)
    }

    override fun setClickable(clickable: Boolean) {
        super.setClickable(clickable)

        root.isClickable = clickable
    }

    override fun setEnabled(enabled: Boolean) {
        super.setEnabled(enabled)

        root.isEnabled = enabled

        when (enabled) {
            true -> root.background =
                ContextCompat.getDrawable(context, R.drawable.round_background)
            false -> root.background =
                ContextCompat.getDrawable(context, R.drawable.round_background_disabled)
        }
    }
}