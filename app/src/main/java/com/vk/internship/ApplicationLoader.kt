package com.vk.internship

import android.app.Application
import android.widget.Toast
import androidx.appcompat.app.AppCompatDelegate
import com.facebook.drawee.backends.pipeline.Fresco
import com.vk.api.sdk.VK
import com.vk.api.sdk.VKTokenExpiredHandler
import com.vk.internship.ui.StartActivity

class ApplicationLoader : Application() {
    override fun onCreate() {
        super.onCreate()

        AndroidUtilities.checkDisplaySize(this)

        AndroidUtilities.fillStatusBarHeight(this)
        AndroidUtilities.fillNavigationBarHeight(this)

        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)

        VK.addTokenExpiredHandler(tokenTracker)

        Fresco.initialize(this)
    }

    private val tokenTracker = object : VKTokenExpiredHandler {
        override fun onTokenExpired() {
            StartActivity.startFrom(this@ApplicationLoader)
        }
    }
}