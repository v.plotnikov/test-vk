package com.vk.internship.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.facebook.drawee.view.SimpleDraweeView
import com.vk.internship.R
import com.vk.internship.models.GroupModel

class GroupsAdapter(private val context: Context) :
    RecyclerView.Adapter<GroupsAdapter.ViewHolder>() {

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val root: LinearLayout = view.findViewById(R.id.item_group)
        val pictureContainer: FrameLayout =
            view.findViewById(R.id.item_group_picture_container)
        val picture: SimpleDraweeView = view.findViewById(R.id.item_group_picture)
        val checkMark: FrameLayout = view.findViewById(R.id.item_group_check_mark)
        val name: TextView = view.findViewById(R.id.item_group_name)
    }

    private var groups: ArrayList<GroupModel> = arrayListOf()
    private var previousSize = 0

    var onClickListener: ((it: GroupModel) -> Unit)? = null
    var onLongClickListener: ((it: GroupModel) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.group_model, parent, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val group = groups[position]
        holder.root.setOnClickListener {
            group.isSelected = !group.isSelected
            notifyItemChanged(position)

            onClickListener?.invoke(group)
        }
        holder.root.setOnLongClickListener {
            onLongClickListener?.invoke(group)

            return@setOnLongClickListener true
        }
        if (!group.name.isNullOrEmpty()) {
            holder.name.text = group.name
        }
        if (!group.image.isNullOrBlank()) {
            holder.picture.setImageURI(group.image)
        }

        setSelection(holder, group.isSelected)
    }

    override fun getItemCount() = groups.size

    fun setGroups(groups: ArrayList<GroupModel>) {
        val localSize = this.groups.size

        this.groups = groups

        val min = minOf(localSize, groups.size)
        val max = maxOf(localSize, groups.size)

        if (localSize < groups.size) {
            notifyItemRangeInserted(min, max)
        } else if (localSize > groups.size) {
            notifyItemRangeRemoved(min, max)
        }

        if (min != max) {
            notifyItemRangeChanged(0, min)
        } else {
            notifyItemRangeInserted(previousSize, max)
            previousSize = max
        }
    }

    fun getGroups(): ArrayList<GroupModel> {
        return groups
    }

    fun insertGroup(group: GroupModel) {
        groups.add(group)
        notifyItemInserted(groups.size - 1)
    }

    fun removeGroup(position: Int) {
        groups.removeAt(position)
        notifyItemRemoved(position)
        notifyItemRangeChanged(position, groups.size)
    }

    private fun setSelection(holder: ViewHolder, isSelection: Boolean) {
        if (isSelection) {
            holder.pictureContainer.foreground =
                ContextCompat.getDrawable(context, R.drawable.circle_selection)
            holder.checkMark.visibility = View.VISIBLE
        } else {
            holder.pictureContainer.foreground =
                ContextCompat.getDrawable(context, R.drawable.circle_default)
            holder.checkMark.visibility = View.INVISIBLE
        }
    }
}