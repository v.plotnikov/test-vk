package com.vk.internship.databases

import androidx.room.Database
import androidx.room.RoomDatabase
import com.vk.internship.models.GroupModel

@Database(entities = [GroupModel::class], version = 1, exportSchema = false)
abstract class GroupModelsDatabase : RoomDatabase() {
    abstract fun GroupModelsDao(): GroupModelsDao
}