package com.vk.internship.databases

import androidx.room.*
import com.vk.internship.models.GroupModel

@Dao
interface GroupModelsDao {
    @Query("SELECT * FROM unsubscribed_groups")
    fun getAll(): List<GroupModel>

    @Insert
    fun insert(groupModel: GroupModel)

    @Update
    fun update(groupModel: GroupModel)

    @Delete
    fun delete(groupModel: GroupModel)
}