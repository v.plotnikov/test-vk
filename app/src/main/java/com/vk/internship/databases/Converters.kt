package com.vk.internship.databases

import androidx.lifecycle.MutableLiveData
import androidx.room.TypeConverter

class Converters {
    @TypeConverter
    fun fromLiveDataInt(data: MutableLiveData<Int>): Int {
        return data.value!!
    }

    @TypeConverter
    fun toLiveDataInt(value: Int): MutableLiveData<Int> {
        return MutableLiveData<Int>(value)
    }
}