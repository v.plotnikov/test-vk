package com.vk.internship.viewmodels

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.vk.api.sdk.VK
import com.vk.api.sdk.VKApiCallback
import com.vk.dto.common.id.UserId
import com.vk.internship.AndroidUtilities
import com.vk.internship.models.GroupModel
import com.vk.sdk.api.groups.GroupsService
import com.vk.sdk.api.groups.dto.*
import com.vk.sdk.api.wall.WallService
import com.vk.sdk.api.wall.dto.WallGetResponse

class GroupsViewModel : ViewModel() {
    private var subscribedGroups = MutableLiveData<ArrayList<GroupModel>>(arrayListOf())

    private var unsubscribedGroups = MutableLiveData<ArrayList<GroupModel>>(arrayListOf())
    private var isUnsubscribedGroupsInit = false

    private var selectedGroupModel: GroupModel? = null
    private var selectedGroupsCount = MutableLiveData(0)
    private var selectedSubscribedGroupsCount = MutableLiveData(0)
    private var selectedUnsubscribedGroupsCount = MutableLiveData(0)

    companion object {
        enum class ActivityMode {
            UNSUBSCRIBE, SUBSCRIBE
        }
    }

    private var activityMode = MutableLiveData(ActivityMode.UNSUBSCRIBE)

    fun getSubscribedGroups(): LiveData<ArrayList<GroupModel>> {
        if (subscribedGroups.value!!.isEmpty()) {
            getUserGroups()
        }
        return subscribedGroups
    }

    fun getUnsubscribedGroups(): LiveData<ArrayList<GroupModel>> {
        return unsubscribedGroups
    }

    fun setUnsubscribedGroups(groups: List<GroupModel>) {
        if (!isUnsubscribedGroupsInit) {
            unsubscribedGroups.value = ArrayList(groups)

            isUnsubscribedGroupsInit = true
        }
    }

    fun getSelectedGroup(): GroupModel? {
        return selectedGroupModel
    }

    fun setSelectedGroup(group: GroupModel) {
        selectedGroupModel = group
    }

    fun getSelectedGroupsCount(): LiveData<Int> {
        return selectedGroupsCount
    }

    fun setSelectedGroupsCount(selected: Int) {
        if (selected >= 0) {
            selectedGroupsCount.value = selected
        }
    }

    fun getActivityMode(): LiveData<ActivityMode> {
        return activityMode
    }

    fun setActivityMode(activityMode: ActivityMode) {
        if (this.activityMode.value != activityMode) {
            this.activityMode.value = activityMode

            if (activityMode == ActivityMode.UNSUBSCRIBE) {
                selectedUnsubscribedGroupsCount.value = selectedGroupsCount.value
                selectedGroupsCount.value = selectedSubscribedGroupsCount.value
            } else {
                selectedSubscribedGroupsCount.value = selectedGroupsCount.value
                selectedGroupsCount.value = selectedUnsubscribedGroupsCount.value
            }
        }
    }

    private fun getUserGroups() {
        VK.execute(
            GroupsService().groupsGetExtended(
                fields = listOf(
                    GroupsFields.DESCRIPTION,
                    GroupsFields.MEMBERS_COUNT
                ),
                offset = 0,
                count = 1000
            ),
            object : VKApiCallback<GroupsGetObjectExtendedResponse> {
                override fun success(result: GroupsGetObjectExtendedResponse) {
                    for (i in result.items) {
                        val group = GroupModel(
                            id = i.id.value,
                            image = i.photo200,
                            name = i.name,
                            description = i.description,
                            membersCount = i.membersCount!!
                        )
                        subscribedGroups.value!!.add(group)
                    }
                    subscribedGroups.value = subscribedGroups.value
                }

                override fun fail(error: Exception) {
                    Log.e(AndroidUtilities.applicationTag, error.toString())
                }
            })
    }

    fun getFriendsCountAndLastPostDate(group: GroupModel) {
        if (group.lastPostDate.value == 0) {
            getFriendsCount(group)
            getLastPostDate(group)
        }
    }

    private fun getFriendsCount(group: GroupModel) {
        VK.execute(
            GroupsService().groupsGetMembers(
                groupId = group.id.toString(),
                count = 0,
                filter = GroupsGetMembersFilter.FRIENDS,
            ), object : VKApiCallback<GroupsGetMembersFieldsResponse> {
                override fun success(result: GroupsGetMembersFieldsResponse) {
                    group.friendsCount.value = result.count
                }

                override fun fail(error: Exception) {
                    Log.e(AndroidUtilities.applicationTag, error.toString())
                }
            })
    }

    private fun getLastPostDate(group: GroupModel) {
        VK.execute(
            WallService().wallGet(ownerId = UserId(-group.id), count = 2),
            object : VKApiCallback<WallGetResponse> {
                override fun success(result: WallGetResponse) {
                    if (result.items.isNotEmpty()) {
                        val ind =
                            if (result.items.size > 1 && result.items[1].date!! > result.items[0].date!!) 1 else 0
                        group.lastPostDate.value = result.items[ind].date!!
                    }
                }

                override fun fail(error: Exception) {
                    Log.e(AndroidUtilities.applicationTag, error.toString())
                }
            })
    }
}