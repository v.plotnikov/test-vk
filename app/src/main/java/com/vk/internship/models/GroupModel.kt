package com.vk.internship.models

import androidx.lifecycle.MutableLiveData
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.vk.internship.databases.Converters

@Entity(tableName = "unsubscribed_groups")
@TypeConverters(Converters::class)
data class GroupModel(
    @PrimaryKey
    val id: Long,
    val image: String? = null,
    val name: String? = null,
    val membersCount: Int = 0,
    var friendsCount: MutableLiveData<Int> = MutableLiveData<Int>(0),
    val description: String? = null,
    var lastPostDate: MutableLiveData<Int> = MutableLiveData<Int>(0),
    var isSelected: Boolean = false
)